const { addArgument } = require('@wdio/allure-reporter').default;
const debug = !!process.env.DEBUG;
const execArgv = debug ? ['--inspect'] : [];
const stepTimout = debug ? 24 * 60 * 60 * 1000 : 100000;
// const capabilities = debug
//     ? [
//         // {   browserName: 'chrome',
//         //     maxInstances: 1
//         // },
//         {
//             browserName: 'firefox',
//             maxInstances: 1,
//
//         }
//
//         ]
//     : [
//           {
//
//               maxInstances: 1,
//               browserName: 'chrome',
//
//               'goog:chromeOptions': {
//
//                   args: [
//                      // '--disable-gpu',
//                       '--window-size=1366,768',
//                       '--disable-software-rasterizer',
//                   ],
//               },
//           },
//
//         {
//             maxInstances: 1,
//             browserName: 'firefox',
//
//             'moz:firefoxOptions': {
//                 // flag to activate Firefox headless mode (see https://github.com/mozilla/geckodriver/blob/master/README.md#firefox-capabilities for more details about moz:firefoxOptions)
//                 // args: ['-headless'],
//             },
//
//
//         }
//
//       ];
const maxInstances = debug ? 1 : 10;
let scenarioCounter = 0;

exports.config = {

    runner: 'local',
    
   path: '/wd/hub',

    port: 4444,

    specs: ['./src/features/login.feature',
            './src/features/signUp.feature',
            './src/features/survey.feature'
    ],

    // Patterns to exclude.
    exclude: [
        // 'path/to/excluded/files'
    ],
   
    // maxInstances: maxInstances,
    maxInstances: 3,
  
    capabilities: [
     //    {
     //    browserName: 'MicrosoftEdge',
     //    browserVersion:'85.0.564.63',
     //    maxInstances: 1,
     // },
     //    {
     //        maxInstances: 1,
     //        browserName: 'firefox',
     //        'moz:firefoxOptions': {
     //
     //        }
     //        },
        {
            maxInstances: 1,
            browserName: 'chrome',
              'goog:chromeOptions': {

                  args: [
                      '--headless',
                      '--disable-gpu',

                      '--window-size=1366,768',
                      '--disable-software-rasterizer',
                  ],
              },

        },

        // {
        //     browserName: 'internet explorer',
        //     maxInstances: 1,
        // }
//         {
//             browserName: "opera",
//           'goog:chromeOptions': {
//           binary: "C:\\Users\\Faiza\\AppData\\Local\\Programs\\Opera\\71.0.3770.171\\opera.exe"
// }
//         }

    ],

    logLevel: 'silent',

    execArgv: execArgv,

    bail: 0,
   
    baseUrl: 'http://localhost',

    waitforTimeout: 60000,

    connectionRetryTimeout: 30000,

    connectionRetryCount: 1,

    services: [
        ['selenium-standalone',
        //  {
        //     logPath: 'logs',
        //     installArgs: {
        //         drivers: {
        //             chrome: { version: '85.0.4183.83' },
        //             // firefox: { version: '0.26.0' },
        //             // edge: {version: '85.0.564.63'},
        //             // ie: {version: '3.14.0'},

        //         }
        //     },
        //     args: {
        //         drivers: {
        //                 chrome: { version: '85.0.4183.83' },
        //             // firefox: { version: '0.26.0' },
        //             // edge: {version: '85.0.564.63'},
        //             // ie: {version: '3.14.0'},
        //         }
        //     },

        // }
        ]
    ],


    framework: 'cucumber',
  
    reporters: [
        'spec',
        [
            'allure',
            {
                outputDir: 'allure-results',
                disableWebdriverStepsReporting: true,
                disableWebdriverScreenshotsReporting: false,
                useCucumberStepReporter: true,
            },
        ],
    ],

    cucumberOpts: {
        backtrace: false, 
        dryRun: false, 
        failFast: false, // <boolean> abort the run on first failure
        format: ['pretty'], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
        colors: true, // <boolean> disable colors in formatter output
        snippets: true, // <boolean> hide step definition snippets for pending steps
        source: true, // <boolean> hide source uris
        profile: [], // <string[]> (name) specify the profile to use
        strict: false, // <boolean> fail if there are any undefined or pending steps
        tagExpression: '', // <string> (expression) only execute the features or scenarios with tags matching the expression
        timeout: stepTimout, // <number> timeout for step definitions
        ignoreUndefinedDefinitions: false, // <boolean> Enable this config to treat undefined definitions as warnings
        // requireModule: [
        //     // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
        //     'tsconfig-paths/register',
        //     () => {
        //         require('ts-node').register({ files: true });
        //     },
        // ],

        requireModule: [
            'tsconfig-paths/register',
        ],
      require: ['./src/stepDefinitions/Login.steps.ts',
          './src/stepDefinitions/Signup.steps.ts',
          './src/stepDefinitions/Surveys.steps.ts' ], // <string[]> (file/dir) require files before executing features
    },

 
    before: function(capabilities, specs) {
        // require('ts-node/register');
        require('ts-node').register({ files: true });
    },

    beforeFeature: function(uri, feature, scenarios) {
        scenarioCounter = 0;
    },
  
    afterStep: function(uri, feature, { error }) {
        if (error !== undefined) {
            browser.takeScreenshot();
        }
    },

    afterScenario: function(uri, feature, scenario, result, sourceLocation) {
        scenarioCounter += 1;
        addArgument('Scenario #', scenarioCounter);
    },
  
};
