import Page from './Page';
import loginpage from '../pages/LoginPage';
class SurveyPage extends Page{

async navigateToSurvey(){
    const survey = await $('button[data-rb-event-key="survey"]')
    await survey.click()
    await expect(browser).toHaveUrl('http://ippsec.virtualforce.io/auth-core/surveys')

}

async clickEditSurvey(){
    const editSurvey = await $('//*[@id="root"]/div[2]/div[2]/main/div/div/div/div/div/div/div/div/div[2]/div/div[3]/div/div[2]/button[1]/i');
    await editSurvey.waitForExist({timeout: 9000, timeoutMsg:"Text is not present or taking too long to display"});
    await editSurvey.click();

    //Wait for survey content to appear
    const surveyContent = await $('//div[@class=\'dbcboxTitle\' and contains(text(),\'Survey Content\')]');
    await surveyContent.waitForExist({timeout: 9000, timeoutMsg:"Text is not present or taking too long to display"});
    await expect(surveyContent).toBeDisplayed()

}
async clickFreeFromText(){
    const freeFromText=await $('//*[@id="root"]/div[2]/div[2]/main/div/div/div/div/div[2]/div[2]/div/div[1]/div[2]/div/div[1]/div/ul/li[1]/button/span/img')
    await freeFromText.waitForExist({timeout: 9000, timeoutMsg:"Text is not present or taking too long to display"})
    await freeFromText.click();

}

async newQuestionScreen(){
    const newScreen = await $('//div[@class=\'modalTitle modal-title h4\' and contains(text(),\'Add New Question\')]')
    await newScreen.waitForExist({timeout: 9000, timeoutMsg:"Text is not present or taking too long to display"});
    await expect(newScreen).toBeDisplayed();
}

async addTitle(title,sheet){
    const getColumn = await loginpage.getCellByName('title',sheet)
    const getRow = await loginpage.getEntireRow(getColumn,title,sheet)
    const getvalue = await loginpage.getValueAgainstColumnKey(getRow,title,sheet)
    console.log(getvalue)

    const enterTitle = await $('//input[@type=\'text\' and @required]')
    await enterTitle.click()
    await enterTitle.setValue(getvalue)
}

async saveAsTag(tag,title,sheet){
    const getColumn = await loginpage.getCellByName('title',sheet)
    const getRow = await loginpage.getEntireRow(getColumn,title,sheet)
    const getvalue = await loginpage.getValueAgainstColumnKey(getRow,tag,sheet)

    const variable = await $('//input[@type=\'text\' and @placeholder=\'Select variable...\']')
    await variable.setValue(getvalue)

    // const selection = await $('//a[contains(text(),\'New selection: \')]');
    // await selection.waitForExist({timeout: 100000, timeoutMsg:"Text is not present or taking too long to display"})
    // await selection.click()



}


    async addDesc(description,title,sheet){

    const getColumn = await loginpage.getCellByName('title',sheet)
    const getRow = await loginpage.getEntireRow(getColumn,title,sheet)
    const getvalue = await loginpage.getValueAgainstColumnKey(getRow,description,sheet)

    await console.log(getvalue)

    const enterTitle = await $('//textarea[@name=\'description\' and @required]')
    await enterTitle.setValue(getvalue)
}

async selectOption(){
    const option = await $("//input[@name='is_required' and @value='false']");
    await  option.click()

}

async minLength(minLen,title,sheet){
    const getColumn = await loginpage.getCellByName('title',sheet)
    const getRow = await loginpage.getEntireRow(getColumn,title,sheet)
    const getvalue = await loginpage.getValueAgainstColumnKey(getRow,minLen,sheet)

    const option = await $("//input[@name='min_length' and @required]");
    await option.setValue(getvalue)

}
async maxLength(maxLen,title,sheet){
    const getColumn = await loginpage.getCellByName('title',sheet)
    const getRow = await loginpage.getEntireRow(getColumn,title,sheet)
    const getvalue = await loginpage.getValueAgainstColumnKey(getRow,maxLen,sheet)
    const option = await $("//input[@name='max_length' and @required]");
    await option.setValue(getvalue)
    await browser.pause(10000)

}

async hitAddButton(){
    const addBtn= await $('//button[@type=\'submit\']')
    await addBtn.click()
    await browser.pause(10000)

}

async verifyQuestion(title,sheet){
    const getColumn = await loginpage.getCellByName('title',sheet)
    const getRow = await loginpage.getEntireRow(getColumn,title,sheet)
    const getvalue = await loginpage.getValueAgainstColumnKey(getRow,title,sheet)
    console.log(getvalue)

    const verify = await $('//div[@class=\'questionsBoxTitle\' and contains(text(),\''+getvalue+'\')]')
    await expect(verify).toBeDisplayed()

}

async previewSurvey(){
    const previewBtn = await $('//button[contains(text(),\'Preview\')]')
    expect(previewBtn).toBeDisplayed()
    await previewBtn.click();
}

async previewScreen(){
    const previewScreenDisplayed = await $('//div[contains(text(),\'Survey Preview\')]')
    await  previewScreenDisplayed.waitForExist({timeout: 6000, timeoutMsg: "preview screen is taking too long to display"})
    await expect(previewScreenDisplayed).toBeDisplayed()
}

}

export default new SurveyPage();