import Page from './Page';
import {Workbook} from "exceljs";
const Excel = require('exceljs');
let wb = new Workbook();

class LoginPage extends Page {
  /**
  * define elements
  */
    //
    // get email() { return  $('input[name="email"]'); }
    // get passw() { return $('input[name="password"]'); }
    //
    // //get loginBtn() {return $('button:nth-child(2)'); }
    //
    // //Actions
    //
    async open () {
        await super.open('http://ippsec.virtualforce.io/signin')    //provide your additional URL if any. this will append to the baseUrl to form complete URL
        await browser.maximizeWindow();
        await browser.pause(10000);

    }

    //***SEARCH FOR COLUMN USING COLUMN NAME

    async getCellByName(arg,sheet){

        let match,rowMatch,store,nameCol;
        await wb.xlsx.readFile('./src/InputData/IPPSecTestData.xlsx')
        let worksheet = wb.getWorksheet(sheet)

        //***SEARCH FOR COLUMN USING COLUMN NAME

        worksheet.eachRow(function (row, rowNumber) {
            row.eachCell(function (cell, colNumber) {
//                   console.log(cell)
                if (cell.value == arg) {
                    nameCol = worksheet.getColumn(colNumber);
                    // assign the values of the name col
                    match = nameCol.values;
                    console.log('column --->' +match);

                }
            });
        })

        return nameCol
    }

    //***ITERATE THROUGH THAT COLUMN TO FIND A PARTICULAR VALUE

    async getEntireRow(arg1,arg2,sheet){
        let rowMatch,values
            await wb.xlsx.readFile('./src/InputData/IPPSecTestData.xlsx')
            let worksheet = wb.getWorksheet(sheet)
         await arg1.eachCell({includeEmpty:true},function (cell, rowNumber) {

             if (cell.value == arg2) {
                 rowMatch = worksheet.getRow(rowNumber)
                 values = rowMatch.values
                 console.log('row ---> : ' , values)

             }
         })
        return values
    }

    //*** LOOP THROUGH TO FIND THE KEY

  async getValueAgainstColumnKey(rowMatch,args,sheet) {
      let store
      await wb.xlsx.readFile('./src/InputData/IPPSecTestData.xlsx')
      let worksheet = wb.getWorksheet(sheet)
      worksheet.eachRow(function (row, rowNumber) {
          row.eachCell(function (cell, colNumber) {//console.log('Cell ' + colNumber + ' = ' + cell.value);

              if (cell.value == args) {
                  //  const nameCol = worksheet.getColumn(colNumber);
                  console.log("column number: " + colNumber)
                  for (let i = 1; i <= colNumber; i++) {
                      store = rowMatch[i]
                      console.log(store)
                  }
              }
          })
      })

      return store

  }


    async invalidEmail(username,domain,sheet){

        const getColumn = await this.getCellByName('username',sheet)
        const getRow = await this.getEntireRow(getColumn,username,sheet)
        const getvalue = await this.getValueAgainstColumnKey(getRow,username,sheet)
        console.log(username)

        //*** Get the domain
        const getDomainColumnValue = await this.getValueAgainstColumnKey(getRow,domain,sheet)

        //*** Function to concatenate username and domain
        const finalValue = await this.concatenateForEmail(getvalue,getDomainColumnValue)

        console.log("concatenated value: "+finalValue)
        const x=await $('input[name="email"]');
        await x.setValue(finalValue);
        await browser.pause(5000)

    }

        async concatenateForEmail(arg1,arg2){
            return await arg1.concat("@", arg2)

        }

    async invalidPass(password,username,sheet){
        const getColumn = await this.getCellByName('username',sheet)
        const getRow = await this.getEntireRow(getColumn,username,sheet);
        const getvalue = await this.getValueAgainstColumnKey(getRow,password,sheet)
        const x=await $('input[name="password"]');
        await x.click()
        await x.setValue(getvalue)
        await browser.pause(9000)


    }


    async logInBtn(){
        const btn = await $('//*[@id="authScreen"]/div[2]/div/div[6]/button');
        await btn.click();
    }

    async emptyEmail(username,domain,sheet){
        const getColumn = await this.getCellByName('username',sheet)
        const getRow = await this.getEntireRow(getColumn,username,sheet)
        const getvalue = await this.getValueAgainstColumnKey(getRow,username,sheet)
        const x=await $('input[name="email"]');
        await x.setValue(getvalue);
    }

    async emptyPassword(password,username,sheet){
        const getColumn = await this.getCellByName('username',sheet)
        const getRow = await this.getEntireRow(getColumn,username,sheet);
        const getvalue = await this.getValueAgainstColumnKey(getRow,password,sheet)
        const x=await $('input[name="password"]');
        await x.click()
        await x.setValue(getvalue)

    }

    async loginButtonDisabled(){

        const btn=await $('//*[@id="authScreen"]/div[2]/div/div[6]/button');
        await expect(btn).toBeDisabled()
    }

    async enterValidEmail(username,domain,sheet){
        const getColumn = await this.getCellByName('username',sheet)
        const getRow = await this.getEntireRow(getColumn,username,sheet)
        const getvalue = await this.getValueAgainstColumnKey(getRow,username,sheet)
        console.log(username)

        //*** Get the domain
        const getDomainColumnValue = await this.getValueAgainstColumnKey(getRow,domain,sheet)

        //*** Function to concatenate username and domain
        const finalValue = await this.concatenateForEmail(getvalue,getDomainColumnValue)

        console.log("concatenated value: "+finalValue)
        const x=await $('input[name="email"]');
        await x.setValue(finalValue);

    }

    async enterValidPassword(password,username,sheet){
        const getColumn = await this.getCellByName('username',sheet)
        const getRow = await this.getEntireRow(getColumn,username,sheet)
        const getvalue = await this.getValueAgainstColumnKey(getRow,password,sheet)
        console.log(username)

        const x=await $('input[name="password"]');
        await x.setValue(getvalue);
        await browser.pause(5000)

    }

    async loginWithValidCred(){
        try{
        const user = await $('p.user-info-name')
        const userValue= await user.getText()
        await user.waitForExist({timeout: 9000, timeoutMsg:"Text " +userValue+"is not present or taking too long to display"})
        await expect(user).toHaveTextContaining("Admin")
    }
    
    catch (e)
    {
        console.log(e);
    }
    }

}
export default new LoginPage();
