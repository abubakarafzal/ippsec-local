import Page from './Page';
import loginpage from '../pages/LoginPage';

class SignUpPage extends Page {

    async clickSignUp(){

        const signupOption =  await $('div>button:nth-child(2)')
        await signupOption.click()

    }

    async navigateToSignUp(){

       await expect(browser).toHaveUrlContaining('http://ippsec.virtualforce.io/signin')
    }

    async enterFirstName(firstname,username,sheet){

        const getColumn = await loginpage.getCellByName('username',sheet)
        const getRow = await loginpage.getEntireRow(getColumn,username,sheet)
        const getvalue = await loginpage.getValueAgainstColumnKey(getRow,firstname,sheet)

        const fname = await $('input[name="firstName"]')
        await fname.setValue(getvalue)
        await browser.pause(6000)

    }

    async enterLastName(lastname,username,sheet){

        const getColumn = await loginpage.getCellByName('username',sheet)
        const getRow = await loginpage.getEntireRow(getColumn,username,sheet)
        const getvalue = await loginpage.getValueAgainstColumnKey(getRow,lastname,sheet)

        const fname = await $('input[name="lastName"]')
        await fname.setValue(getvalue)
        await browser.pause(6000)

    }

    async enterEmail(username,domain,sheet){

        const getColumn = await loginpage.getCellByName('username',sheet)
        const getRow = await loginpage.getEntireRow(getColumn,username,sheet)
        const getvalue = await loginpage.getValueAgainstColumnKey(getRow,username,sheet)
        console.log(username)

        //*** Get the domain
        const getDomainColumnValue = await loginpage.getValueAgainstColumnKey(getRow,domain,sheet)

        //*** Function to concatenate username and domain
        const finalValue = await loginpage.concatenateForEmail(getvalue,getDomainColumnValue)

        console.log("concatenated value: "+finalValue)
        const x=await $('input[name="email"]');
        await x.setValue(finalValue);
        await browser.pause(5000)
    }


    async enterPassword(password,username,sheet){
        const getColumn = await loginpage.getCellByName('username',sheet)
        const getRow = await loginpage.getEntireRow(getColumn,username,sheet);
        const getvalue = await loginpage.getValueAgainstColumnKey(getRow,password,sheet)

        const x=await $('input[type=\'password\']');
        console.log(getvalue)
        await x.click()
        await x.setValue(getvalue)
        await browser.pause(9000)
    }

    async errorMessage(message,username,sheet){
        const getColumn = await loginpage.getCellByName('username',sheet)
        const getRow = await loginpage.getEntireRow(getColumn,username,sheet);
        const getvalue = await loginpage.getValueAgainstColumnKey(getRow,message,sheet)

        console.log("Error message:  "+ getvalue)
        const msg = await $('small:nth-child(2)')
        const  text = await msg.getText()
        console.log("Text is: "+text)
        await expect(msg).toHaveText(getvalue)

    }

    async errMsgForPassword(message,username,sheet){
        const getColumn = await loginpage.getCellByName('username',sheet)
        const getRow = await loginpage.getEntireRow(getColumn,username,sheet);
        const getvalue = await loginpage.getValueAgainstColumnKey(getRow,message,sheet)
        console.log("Error message:  "+ getvalue)
        const msg = await $('small:nth-child(3)')
        const  text = await msg.getText()
        console.log("Text is: "+text)
        await expect(msg).toHaveText(getvalue)
    }
}

export default new SignUpPage();