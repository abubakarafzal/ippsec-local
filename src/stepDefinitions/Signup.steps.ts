import { assert } from 'chai';
import loginpage from '../pages/LoginPage';
import signuppage from '../pages/SignUpPage';
import {Given} from 'cucumber'
import {When} from 'cucumber'
import {Then} from 'cucumber'

//****  Invalid email

Given(/^I am on IPPSec site$/,{timeout: 6 * 10000}, async function () {
    await loginpage.open();
    const title =  await browser.getTitle();
    await expect(browser).toHaveTitle("ASPECT")
});


When(/^I Click on Signup$/,{timeout: 6 * 10000},async function () {
  await signuppage.clickSignUp()
});


Then(/^I navigate to signup page$/,{timeout: 6 * 10000}, async function () {
    await signuppage.navigateToSignUp()
});


Then(/^I enter "([^"]*)" of "([^"]*)" "([^"]*)"$/,{timeout: 6 * 10000}, async function (firstname,username,sheet) {
   await signuppage.enterFirstName(firstname,username,sheet)
});


Then(/^I am entering the "([^"]*)" of "([^"]*)" "([^"]*)"$/,{timeout: 6 * 10000}, async function (lastname,username,sheet) {
    await signuppage.enterLastName(lastname, username, sheet)
});


Then(/^I enter an invalid email of "([^"]*)" "([^"]*)" "([^"]*)"$/,{timeout: 6 * 10000}, async function (username,domain,sheet) {
    await signuppage.enterEmail(username,domain,sheet)
});


Then(/^I enter the password "([^"]*)" of "([^"]*)" "([^"]*)"$/,{timeout: 6 * 10000}, async function (password,username,sheet) {
    await signuppage.enterPassword(password,username,sheet)
});


Then(/^I should be able see invalid email "([^"]*)" "([^"]*)" "([^"]*)"$/,{timeout: 6 * 10000}, async function (message,username,sheet) {
    await signuppage.errorMessage(message,username,sheet)
});


//****  Invalid password ****

Then(/^I am entering "([^"]*)" "([^"]*)" "([^"]*)"$/,{timeout: 6 * 10000}, async function (firstname,username,sheet) {
    await signuppage.enterFirstName(firstname,username,sheet)
});


Then(/^I am specify the "([^"]*)" "([^"]*)" "([^"]*)"$/,{timeout: 6 * 10000}, async function (lastname,username,sheet) {
    await signuppage.enterLastName(lastname, username, sheet)
});


Then(/^I am entering a valid email "([^"]*)" "([^"]*)" "([^"]*)"$/,{timeout: 6 * 10000}, async function (username,domain,sheet) {
    await signuppage.enterEmail(username,domain,sheet)
});


Then(/^I enter an invalid "([^"]*)" "([^"]*)" "([^"]*)"$/,{timeout: 6 * 10000}, async function (password,username,sheet) {
    await signuppage.enterPassword(password,username,sheet)
});

Then(/^I see invalid password "([^"]*)" "([^"]*)" "([^"]*)"$/,{timeout: 6 * 10000}, async function (message,username,sheet) {
    await signuppage.errMsgForPassword(message,username,sheet)
});
