import { assert } from 'chai';
import { binding, given, when, then } from 'cucumber-tsflow';
import loginpage from '../pages/LoginPage';
import {When} from 'cucumber'
import {Then} from 'cucumber'
import {Given} from 'cucumber'



        //*** Sign in with invalid Credentials ***

Given(/^I am on login page of IPPSec$/, async function () {
     await loginpage.open();
     const title =  await browser.getTitle();
     await expect(browser).toHaveTitle("ASPECT")
});

When(/^I am entering email "([^"]*)","([^"]*)" "([^"]*)"$/, async function(username,domain,sheet) {
    await loginpage.invalidEmail(username,domain,sheet)
});

When(/^Enter the "([^"]*)" for "([^"]*)" "([^"]*)"$/, async function (password,username,sheet) {
    await loginpage.invalidPass(password,username,sheet)
});

When(/^I click the login button$/, async function () {
    await loginpage.logInBtn();
});

Then(/^I should be unable to sign in$/, async function () {
    const title =  await browser.getTitle();
        if(title == 'Mansoor'){
            assert.isTrue(false)
        }
        else {
            assert.isTrue(true)
        }
});



        //*** Sign in with empty fields ***

When(/^I enter empty email "([^"]*)","([^"]*)","([^"]*)"$/, async function (username,domain,sheet) {
    await loginpage.emptyEmail(username,domain,sheet)
});

When(/^I am entering empty "([^"]*)" for "([^"]*)" "([^"]*)"$/, async function (password,username,sheet) {
    await loginpage.emptyPassword(password,username,sheet)
});

Then(/^Login button shoud be disabled$/,async function () {
    await loginpage.loginButtonDisabled()
});



        // *** Sign in with valid credentials ***

When(/^I am entering valid email "([^"]*)" "([^"]*)" "([^"]*)"$/, async function (username,domain,sheet) {
    await loginpage.enterValidEmail(username,domain,sheet)
});

When(/^I enter the valid "([^"]*)" for "([^"]*)" "([^"]*)"$/,async function (password,username,sheet) {
    await loginpage.enterValidPassword(password,username,sheet)
});

Then(/^I should be able to login in$/, async function () {
    await loginpage.loginWithValidCred()
});
