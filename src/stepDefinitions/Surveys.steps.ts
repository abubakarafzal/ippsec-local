import loginpage from '../pages/LoginPage';
import surveyPage from '../pages/SurveyPage';
import {Given} from 'cucumber'
import {When} from 'cucumber'
import {Then} from 'cucumber'


Given(/^I enter email (.*) "([^"]*)" "([^"]*)"$/, async function (username,domain,sheet) {
    await loginpage.enterValidEmail(username, domain, sheet)
});


Given(/^I enter the "([^"]*)" (.*) "([^"]*)"$/, async function (password,username,sheet) {
    await loginpage.enterValidPassword(password,username,sheet)
});


Given(/^I hit sign in button and redirect to map page\.$/, async function () {
    await loginpage.logInBtn()
    await loginpage.loginWithValidCred()
});


Given(/^I navigate to survey screen$/,async function () {
    await surveyPage.navigateToSurvey()
});


When(/^I click Edit survey$/, async function () {
    await surveyPage.clickEditSurvey()
});


When(/^From left box,I click on Free from text option$/, async function () {
    await surveyPage.clickFreeFromText()
});


Then(/^Add new question screen will appear$/, async function () {
    await surveyPage.newQuestionScreen()
});


When(/^I add Question title "([^"]*)" "([^"]*)"$/, async function (title,sheet) {
    await surveyPage.addTitle(title,sheet)
});

When(/^I enter tag for "([^"]*)" "([^"]*)" "([^"]*)"$/, async function (tag,title,sheet) {
    await surveyPage.saveAsTag(tag,title,sheet)
});

When(/^I add a Question "([^"]*)" for "([^"]*)" "([^"]*)"$/, async function (description,title,sheet) {
    await surveyPage.addDesc(description,title,sheet)
});


When(/^I select option$/, async function () {
    await surveyPage.selectOption()
});

When(/^I enter minimum length "([^"]*)" "([^"]*)" "([^"]*)"$/, async function (minLen,title,sheet) {
    await surveyPage.minLength(minLen,title,sheet)
});


When(/^I enter maximum length "([^"]*)" "([^"]*)" "([^"]*)"$/, async function (maxLen,title,sheet) {
    await surveyPage.maxLength(maxLen,title,sheet)
});


Then(/^I hit add question$/, async function () {
    await surveyPage.hitAddButton()
});

Then(/^Question should be added and displayed under the survey "([^"]*)" "([^"]*)"$/, async function (title,sheet) {
    await surveyPage.verifyQuestion(title,sheet)
});

When(/^I hit preview button$/, async function () {
    await surveyPage.previewSurvey()
});

Then(/^I should see survey preview screen$/, async function () {
    await surveyPage.previewScreen()
});