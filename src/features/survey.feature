Feature: Surveys

  Background: Open site
    Given I am on IPPSec site

  Scenario Outline: Create survey question with description and free form text


        Given I enter email <username> "Domain" "loginSheet"
        Given I enter the "password" <username> "loginSheet"
        Given I hit sign in button and redirect to map page.
        Given I navigate to survey screen
        When I click Edit survey
        When From left box,I click on Free from text option
        Then Add new question screen will appear
        When I add Question title "<title>" "<sheet>"
        When I enter tag for "save as" "<title>" "<sheet>"
        And I add a Question "description" for "<title>" "<sheet>"
        And I select option
        And I enter minimum length "min length" "<title>" "<sheet>"
        And I enter maximum length "max length" "<title>" "<sheet>"
        Then I hit add question
#       Then Question should be added and displayed under the survey "<title>" "<sheet>"
#        When I hit preview button
#        Then I should see survey preview screen


    Examples:
      | username   |  title       | sheet       |
      | default    |  Test title1 | Survey      |

