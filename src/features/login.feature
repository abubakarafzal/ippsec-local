Feature: Sign in

  Background: Login
    Given I am on login page of IPPSec

Scenario Outline: Sign in with invalid email & password

      When I am entering email "<username>","Domain" "loginSheet"
      When Enter the "password" for "<username>" "loginSheet"
      And  I click the login button
      Then I should be unable to sign in

    Examples:
        |  username        |
        |  mansoor         |
        |  mansoorahmed    |
        |  mansoor.ahmad   |


Scenario Outline: Sign in with empty fields

      When I enter empty email "<username>","Domain","loginSheet"
      When I am entering empty "password" for "<username>" "loginSheet"
      Then Login button shoud be disabled

  Examples:
      | username    |
      |             |

Scenario Outline: Sign in with valid credentials

      When I am entering valid email "<username>" "Domain" "loginSheet"
      And I enter the valid "password" for "<username>" "loginSheet"
      And  I click the login button
      Then I should be able to login in

    Examples:
      |  username    |
      |  default     |





