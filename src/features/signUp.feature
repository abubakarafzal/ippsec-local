Feature: Sign Up

      Background: Signup
        Given I am on IPPSec site

  Scenario Outline: Sign up with invalid email

    When I Click on Signup
    Then I navigate to signup page
    Then I enter "firstname" of "<username>" "<sheet>"
    Then I am entering the "lastname" of "<username>" "<sheet>"
    Then I enter an invalid email of "<username>" "Domain" "<sheet>"
    Then I enter the password "password" of "<username>" "<sheet>"
    Then I should be able see invalid email "error message" "<username>" "<sheet>"

    Examples:
        |  username     | sheet   |
        |  MansoorA     | SignUp  |


  Scenario Outline:Sign up with invalid password

    When I Click on Signup
    Then I navigate to signup page
    Then I am entering "firstname" "<username>" "<sheet>"
    Then I am specify the "lastname" "<username>" "<sheet>"
    Then I am entering a valid email "<username>" "Domain" "<sheet>"
    Then I enter an invalid "password" "<username>" "<sheet>"
    Then I see invalid password "error message" "<username>" "<sheet>"

    Examples:
      |  username     | sheet   |
      |  John123      | SignUp  |
